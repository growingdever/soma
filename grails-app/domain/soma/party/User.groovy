package soma.party

class User {

    static constraints = {
        name blank: false
        email email: true
        kind blank: false
        state blank: false
    }


    String name;
    String email;
    Date createdAt;
    String kind;
    Integer ordinal;
    String state;
    Boolean paid;


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", createdAt=" + createdAt +
                ", kind='" + kind + '\'' +
                ", ordinal=" + ordinal +
                ", state='" + state + '\'' +
                ", paid=" + paid +
                ", version=" + version +
                '}';
    }
}
