<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>

<!-- Header -->
<div class="intro-header">

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="intro-message">
                    <h1>2014 소마 전기수 송년회</h1>

                    <h3>부제: 나도 연말에 불러주는데 있다.</h3>
                    <hr class="intro-divider">
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->

</div>
<!-- /.intro-header -->

<!-- Page Content -->

<div class="content-section-a" id="intro">

    <div class="container">

        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <hr class="section-heading-spacer">

                <div class="clearfix"></div>

                <h2 class="section-heading">안내:</h2>

                <p class="lead">
                <dl class="dl-horizontal" style="font-size:18px;font-weight:400">
                    <dt>일시:</dt><dd>12/20(토) 19시부터 23시까지</dd>
                    <dt>장소:</dt><dd>카페 알베르</dd>
                    <dt>회비:</dt>
                    <dd>직장인 5만원 / 학생 3만원 <br/>
                        <span style="font-size: 16px">국민 9-20132317-56 김원재</span> <br/>
                        <span style="font-size: 14px;opacity:0.5">1기 관습법에 의거, 멘토님도 직장인!<br/>더 내시면, 총무 용돈으로 쓰겠습니다.
                        </span>
                    </dd>
                    <dt>대상:</dt> <dd>소마 관계자 모두</dd>
                    <dt>내용:</dt><dd>각종 B급 개발자 개그 및 <br/> 다양한 떡밥 계속 준비 중</dd>
                    <dt>드레스코드:</dt><dd><span style="">상의와 하의는 필참바람</span></dd>
                </dl>
            </p>
            </div>

            <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                <img class="img-responsive" src="${asset.assetPath(src: 'ipad.png')}" alt="">
            </div>
        </div>

    </div>
    <!-- /.container -->

</div>
<!-- /.content-section-a -->

<div class="content-section-b" id="enroll">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                <hr class="section-heading-spacer">

                <div class="clearfix"></div>

                <h2 class="section-heading">사전신청<span style="">입금</span></h2>

                <p class="lead">
                    국민 9-20132317-56 김원재<br/>
                    <br/>
                    사전 신청수가 많을 수록, <br/>
                    사전 입금자가 많을 수록, <br/>
                    더욱 방탕한 즐거움을 함께할 수 있습니다.<br/>
                    <br/>
                    불참자와 사전 등록 안하고 오시는 분 중에서,<br/>
                    내년 행사 준비 노비를 선발하도록 하겠습니다.<br/>
                    <br/>
                    <span style="opacity:0.5; font-weight: lighter">물론 기본 참석료에서 만원 더 받을겁니다.</span>
                </p>
            </div>

            <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                <g:if test="${!user || user.hasErrors()}">
                    <g:form id="enrollForm" role="form" controller="user" action="enroll" method="POST">
                        <div class="form-group">
                            <label for="inputName">신청자명</label>
                            <input type="text" name="name" class="form-control" id="inputName" value="${user?.name}" placeholder="본명, 본명으로 대동단결">

                            <p class="help-block bg-danger">
                                <g:fieldError field="name" bean="${user}" class="help-block bg-danger"/>
                            </p>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail">이메일</label>
                            <input type="email" name="email" class="form-control" id="inputEmail"
                                   placeholder="입금확인발송 및 소마관련스팸 발송드림)" value="${user?.email}">

                            <p class="help-block bg-danger">
                                <g:fieldError field="email" bean="${user}" class="help-block bg-danger"/>
                            </p>
                        </div>

                        <div class="form-group">
                            <label for="inputKind">구분</label>
                            <g:select name="kind" class="form-control" id="inputKind" from="['멘티', '멘토', '사무국']"/>
                        </div>

                        <div class="form-group">
                            <label for="inputKind">기수</label>
                            <g:select name="ordinal" class="form-control" id="inputKind" from="[1, 2, 3, 4, 5]"/>
                        </div>

                        <div class="form-group">
                            <label for="inputState">한줄현황</label>
                            <input type="text" name="state" class="form-control"  id="inputState" value="${user?.state}" placeholder="ex) D사 노비 개발자 256호">

                            <p class="help-block bg-danger">
                                <g:fieldError field="state" bean="${user}" class="help-block bg-danger"/>
                            </p>
                        </div>

                        <button type="submit" class="btn btn-success">옛다 개인정보 받아라</button>
                    </g:form>
                </g:if>
                <g:else>
                    신청 완료되었습니다. <br/>
                    입금이 확인되는데로, 개인 메일로 알려드리겠습니다.
                </g:else>
            </div>
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.content-section-b -->

<div class="content-section-a" id="way">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <hr class="section-heading-spacer">

                <div class="clearfix"></div>

                <h2 class="section-heading">오시는길</h2>

                <p class="lead">
                    한국의 자랑 첨단 의학 단지,<br/>
                    강남역으로 오시면 됩니다. <br/>
                    <br/>
                    죽어도 못 찾으시겠다면,<br/>
                    강남역에 다양한 종류의 사람들을 배치하였사오니<br/>
                    취향에 맞는 분을 찾아서 물어보시길 권합니다.<br/>
                    <br/>

                    <span style="opacity: 0.5;font-size: 14px">
                        굳이... 인간 남캐에게 문의하시려면, <br/>
                        1기 연락노예 이동훈 010-5387-4227으로 주세요.<br/>
                    </span>

                    <span style="opacity: 0.3;font-size: 12px; text-decoration:line-through">
                        전화 주시는 분이 인간남캐면 안 받을 수도 있습니다
                    </span>

                </p>
            </div>

            <div class="col-lg-5 col-lg-offset-2 col-sm-6" id="map" style="height: 380px">
            </div>
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.content-section-a -->


<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="copyright text-muted small">Copyright &copy; 소마인사이드 2014. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>

%{--로컬 테스트용--}%
%{--<script type="text/javascript" src="https://apis.daum.net/maps/maps3.js?apikey=c522a724994f3d4fad8a7211d37f1ce773bee12f&libraries=services"></script>--}%
<script type="text/javascript" src="https://apis.daum.net/maps/maps3.js?apikey=bd35343030c0f54c7f0a85ce271c0de337eb33c5&libraries=services"></script>
<script>
    var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
    var options = { //지도를 생성할 때 필요한 기본 옵션
        center: new daum.maps.LatLng(33.450701, 126.570667), //지도의 중심좌표.
        level: 3 //지도의 레벨(확대, 축소 정도)
    };

    var map = new daum.maps.Map(container, options);

    // 주소-좌표 변환 객체를 생성합니다
    var geocoder = new daum.maps.services.Geocoder();

    // 주소로 좌표를 검색합니다
    geocoder.addr2coord('서울 강남구 강남대로102길 34', function (status, result) {

        // 정상적으로 검색이 완료됐으면
        if (status === daum.maps.services.Status.OK) {

            var coords = new daum.maps.LatLng(result.addr[0].lat, result.addr[0].lng);

            // 결과값으로 받은 위치를 마커로 표시합니다
            var marker = new daum.maps.Marker({
                map: map,
                position: coords
            });

            // 인포윈도우로 장소에 대한 설명을 표시합니다
            var infowindow = new daum.maps.InfoWindow({
                content: '<div style="padding:2px;">카페 알베르</div>'
            });
            infowindow.open(map, marker);
        }
    });

    $(document).ready(function () {
        var needToMove = ${user && user?.errors};

        if(needToMove) {
            $("html, body").animate({ scrollTop: $("#enroll").position().top }, "fast");
        }
    });

</script>
</body>
</html>