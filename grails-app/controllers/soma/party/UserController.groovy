package soma.party

class UserController {

    def index() {
        render("OK")
    }

    def enroll() {

        def user = new User(params)
        user.paid = false;
        user.createdAt = new Date();

        if (user.validate()) {
            user.save(flush: true, failOnError: true)
        }

        return render(view: '/index', model: [user: user])
    }
}
